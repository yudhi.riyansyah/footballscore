<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'MatchController@index')->middleware('auth');
Route::get('/', 'MatchController@MatchResult');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/league/{id}', 'StandingsController@index');
Route::get('/league/teams/{id}', 'StandingsController@team_details');

Route::get('/match', 'MatchController@MatchResult');
Route::get('/match/{id}', 'MatchController@MatchStat');

Route::get('/news/{country}', 'NewsController@news');
Route::get('/news/{country}/{id}', 'NewsController@newsDetail');

Route::get('/video', 'VideoController@index');
Route::post('/video', 'VideoController@store');

Route::get('/favourite/{user_id}', 'FavouriteController@index')->middleware('auth');
Route::delete('/favourite/{user_id}/delete/{id}', 'FavouriteController@destroy')->middleware('auth');
