@extends('layouts.master_admin')
@section('content')
<div id="content" class="news_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
		<div class=" standing card main-card">	

			<center><p><h2 class="judul">Sport News</p></h2></center>

			<p class="update">Last Update : {{ $news-> articles[2]->publishedAt}}</p><br>
			<hr>

			@foreach ( $news->articles as $key=>$article)
			<section class="bg-news  mx-5">    	
				<div class="row ">
					<div class="col-sm-3 ml-2"><br>
						<a href="{{$article->url}}" target="_blank"><img  src="{{ $article->urlToImage}}" width="200" height="200" class="center"></a>
					</div>
					<div class="col-sm-8">
						<!-- <a href="/news/{{ Request::segment(2) }}/{{$key}}">{{$article->title}}</a> -->
						<a class="description" href="{{$article->url}}" target="_blank"><br><br><b><h4>{{$article->title}}</h4></b></a>
						{{$article->description}}
					</div>
				</div>
				<hr class="hr-color">
			</section>   
			@endforeach
		</div>
		<br>

		<!--Running Text  -->
		<?php $runningText=''; 
			foreach($news->articles as $key=>$article )
				{
					$runningText .=$article->description . "&nbsp;&nbsp; &nbsp; || &nbsp;&nbsp; &nbsp;";
				}
		?>

		<div class="demo fixed-bottom running-text">
		<marquee behavior="scroll" scrollamount="7" direction="center" width="">
			<p class="tul">{{$runningText}}</p>
		</marquee>
		</div>
	</div>
</div>



@endsection