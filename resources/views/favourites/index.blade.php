{{-- favourite videos --}}
@extends('layouts.master_admin')
@section('css')

@endsection

@section('content')
<!-- Main Content -->
<div id="content" class="favourite_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        <div class=" favourite card main-card"> 
            <ul class="row sortable" style="list-style: none;">
                
                @foreach ( $favouriteVideos as $favouriteVideo)
                    {{-- @php
                        if($key==6) break;
                    @endphp --}}

                {{-- {{$favouriteVideo}}  --}}

                <li class="cardBox col-md-4  ">
                    <div class="card video-card" style="width: 22rem;"> 
                        <form action="{{url('favourite/'.$favouriteVideo->user_id.'/delete'.'/'.$favouriteVideo->id)}}" method="post">
                            <div class="row">
                                <div class="col-md-10 ">
                                    <h6><a target="_blank" name="video_title" href="{{$favouriteVideo->video_url}}">{{$favouriteVideo->title}} </a> </h6> 
                                </div>

                                <div class="col-md-2 text-right">
                                    <button  class="btn submit-btn"  type="submit"> <i class="card_icon fas fa-trash"></i></button>
                                </div>

                            </div>
                            <div name="video_embed">
                                {!!  
                                    $favouriteVideo->video_embed
                                !!}
                            </div>
                            <input name="id" type="text" style="display:none;" value="{{$favouriteVideo->id}}">
                            {{-- <input name="title" type="text" style="display:none;" value="{{$favouriteVideo->title}}">
                            <input name="video_embed" type="text" style="display:none;" value="{{$favouriteVideo->videos[0]->embed}}">
                            <input name="user_id" type="text" style="display:none;" value="{{ Auth::user()->id }} "> --}}

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                    </div>
                </li>
                @endforeach
                
            </ul>
            {{ $favouriteVideos->links() }}
            
            
        </div>
        
        @include('partials.running_text')
        {{-- ===== --}}
    </div>
</div>
@endsection

@section('javascript')
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
    $(document).ready(function () {
                $(".sortable").sortable();
                });
</script>

@endsection