    {{-- <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion " id="accordionSidebar"> --}}
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
          {{-- <i class="fas fa-laugh-wink "></i> --}}
          <i class="fas fa-futbol"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Football<sup>Score</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/">
          {{-- <i class="fas fa-fw fa-tachometer-alt"></i> --}}
          {{-- <i class="fas fa-home"></i> --}}
          <i class="fas fa-flag"></i>
          <span>Latest Match</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Match
      </div>
      <!-- Nav Item - Pages Collapse Menu -->
      {{-- League --}}
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLeague" aria-expanded="true" aria-controls="collapseTwo">
          {{-- <i class="fas fa-fw fa-cog"></i> --}}
          {{-- <i class="fas fa-user-ninja"></i> --}}
          {{-- <i class="far fa-table"></i> --}}
          <i class="fas fa-table"></i>
          <span>League</span>
        </a>
        <div id="collapseLeague" class="collapse" aria-labelledby="headingLeague" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            {{-- <h6 class="collapse-header">Custom Components:</h6> --}}
            {{-- <a class="collapse-item" href="/league/2085">European Championship</a> --}}
            <a class="collapse-item" href="/league/2021">English Premier League</a>
            {{-- <a class="collapse-item" href="/league/2012">Bundesliga</a> --}}
            <a class="collapse-item" href="/league/2019">Italian Serie A</a>
            <a class="collapse-item" href="/league/2014">La Liga Santander</a>
            <a class="collapse-item" href="/league/2003">Eredivisie</a>
            <a class="collapse-item" href="/league/2015">France Ligue 1</a>
            <a class="collapse-item" href="/league/2013">Brazil Serie A</a>
            <a class="collapse-item" href="/league/2017">Portugal Primeira Liga</a>
            {{-- <a class="collapse-item" href="/league/2000">World Cup</a>
            <a class="collapse-item" href="/league/2001">UEFA Champions League</a> --}}
          </div>
        </div>
      </li>

      {{-- news --}}
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNews" aria-expanded="true" aria-controls="collapseUtilities">
          {{-- <i class="fas fa-fw fa-wrench"></i> --}}
          <i class="fas fa-newspaper"></i>
          <span>News</span>
        </a>
        <div id="collapseNews" class="collapse" aria-labelledby="headingNews" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            {{-- <h6 class="collapse-header">Sport News:</h6> --}}
            <a class="collapse-item" href="/news/us">US</a>
            <a class="collapse-item" href="/news/id">Indonesia</a>
          </div>
        </div>
      </li>

    {{-- Highlight Video --}}
        <li class="nav-item">
        <a class="nav-link" href="/video">
          {{-- <i class="fas fa-fw fa-tachometer-alt"></i> --}}
          {{-- <i class="fas fa-home"></i> --}}
          <i class="fas fa-play"></i>
          <span>Highlight</span></a>
      </li>
      {{-- Highlight Video --}}

      {{-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVideo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-user-ninja"></i>
          <span>Highlight</span>
        </a>
        <div id="collapseVideo" class="collapse" aria-labelledby="headingVideo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="/video">Highlight</a>
            <a class="collapse-item" href="/">Latest Match</a>
          </div>
        </div>
      </li> --}}
      
      <!-- Divider -->
      <hr class="sidebar-divider">

      @guest

      @else
      <li class="nav-item active">
        <a class="nav-link" href="/favourite/{{ Auth::user()->id }}">
          {{-- <i class="fas fa-fw fa-folder"></i> --}}
          <i class="fas fa-heart"></i>
          <span>Favourite</span></a>
      </li>
      @endguest


      {{-- <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Colors</a>
            <a class="collapse-item" href="utilities-border.html">Borders</a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
          </div>
        </div>
      </li> --}}

      {{-- <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
          </div>
        </div>
      </li>
{{--
      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
--}}
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        {{-- <button class="rounded-circle border-0" id="sidebarToggle"></button> --}}
      </div> 

    </ul>