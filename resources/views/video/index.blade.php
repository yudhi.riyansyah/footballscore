{{-- highlight videos --}}
@extends('layouts.master_admin')
@section('css')

@endsection

@section('content')
<div id="content" class="video_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        <div class=" highlight card main-card"> 
            <ul class="row sortable" style="list-style: none;">
                @foreach ( $highlightVideos as $key=>$highlightVideo)
                    @php
                        if($key==6) break;
                    @endphp

                <li class="cardBox col-md-4  ">
                    <div class="card video-card" style="width: 22rem;"> 
                        <form action="{{url('video')}}" method="post">
                            <div class="row">
                                <div class="col-md-10 ">
                                    <h6><a target="_blank" name="video_title" href="{{$highlightVideo->url}}">{{$highlightVideo->title}} </a> </h6> 
                                </div>
                                @guest

                                @else
                                <div class="col-md-2 text-right">
                                    <button  class="btn submit-btn"  type="submit"> <i  class="card_icon far fa-heart"></i></button>
                                </div>
                                @endguest
                            </div>
                            <div name="video_embed">
                                {!!  
                                    $highlightVideo->videos[0]->embed
                                    // $highlightVideo->embed
                                !!}
                            </div>
                            <input name="video_url" type="text" style="display:none;" value="{{$highlightVideo->url}}">
                            <input name="title" type="text" style="display:none;" value="{{$highlightVideo->title}}">
                            <input name="video_embed" type="text" style="display:none;" value="{{$highlightVideo->videos[0]->embed}}">
                            @guest

                            @else
                            <input name="user_id" type="text" style="display:none;" value="{{ Auth::user()->id }} ">
                            @endguest
                                

                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
                @endforeach
            </ul>

        </div>
    </div>
</div>

@include('partials.running_text')

@endsection

@section('javascript')
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
    $(document).ready(function () {
                $(".sortable").sortable();
                });
</script>

@endsection