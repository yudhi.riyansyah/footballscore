@extends('layouts.master_admin')
@section('content')
<div id="content" class="standing_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        <div class="card standing-detail">
            <div class="row">
                <div class="col-xl-12 col-md-6 mb-4 text-center title">
                    <h1>{{$team_details->name}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6 mb-4">
                    <img src="{{$team_details->crestUrl}}" width="250px" height="250px" alt="">
                </div>
                <div class="col-xl-9 col-md-6 mb-4">
                    <p>Shortname : {{$team_details->shortName}} </p>
                    <p>Founded : {{$team_details->founded}} </p>
                    <p>Stadium : {{$team_details->venue}} </p>
                    <p>Address : {{$team_details->address}} </p>
                    <p>Phone : {{$team_details->phone}} </p>
                    <p>Email : {{$team_details->email}} </p>
                    <p>Website : <a href="{{$team_details->website}}" target="_blank">{{$team_details->website}}</a>  </p>
                </div>
            </div>
        </div>
    </div>
</div>

@include('partials.running_text')

@endsection
