@extends('layouts.master_admin')
@section('css')
    {{-- <link href="{{url('css/custom.css')}}" rel="stylesheet"> --}}
@endsection

@section('content')
<div id="content" class="standing_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        @php
            $lastUpdated = substr($leagueStandings->competition->lastUpdated,0,10)
        @endphp
        <div class="standing card main-card">
            <h1 class="title">{{$leagueStandings->competition->name}}</h1>
            <p>Last Update : {{$lastUpdated}} </p>
            <table class="table table-striped standing-table">
                <tr>
                    <th>No.</th>
                    <th>Team</th>
                    <th>Won</th>
                    <th>Draw</th>
                    <th>Lost</th>
                    <th>GF</th>
                    <th>GA</th>
                    <th>GD</th>
                    <th>Points</th>
                </tr>
                @foreach ( $leagueStandings->standings[0]->table as $table)
                    <tr>
                        <td> {{$table->position}}</td>
                        <td><img src="{{$table->team->crestUrl}}" width="25px" height="25px" alt=""> <a href="/league/teams/{{$table->team->id}}">{{$table->team->name}}</a></td>
                        <td>{{$table->won}}</td>
                        <td>{{$table->draw}}</td>
                        <td>{{$table->lost}}</td>
                        <td>{{$table->goalsFor}}</td>
                        <td>{{$table->goalsAgainst}}</td>
                        <td>{{$table->goalsAgainst}}</td>
                        <td>{{$table->points}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>


<!--Running Text  -->
@include('partials.running_text')
   

@endsection
