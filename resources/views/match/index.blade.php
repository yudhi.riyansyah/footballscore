@extends('layouts.master_admin')
@section('content')
<div id="content" class="match_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        <div class=" today_match card main-card">
            <div class="container">
                <div class="row match-row">
                    <div class="col-md-1 blank-div"></div>
                    <div class="col-md-5 card today-card">
                        <h1 class="title"> LATEST MATCH </h3>
                        @foreach($todayMatchs->matches as $match)
                        <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">{{ $match->competition->name }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="match-score">
                                <td  scope="row"> 
                                    {{ $match->homeTeam->name }} 
                                    {{-- <a href="#" target="popup" onClick="window.open('/match/{{$match->id}}','popup',width=370,height=260); return false;">
                                        ( {{ $match->score->fullTime->homeTeam }} - {{ $match->score->fullTime->awayTeam }} )
                                    </a>  --}}
                                    <a href="/match/{{$match->id}}" >
                                        ( {{ $match->score->fullTime->homeTeam }} - {{ $match->score->fullTime->awayTeam }} )
                                    </a> 
                                    {{ $match->awayTeam->name }}
                                </td>
                            </tr>
                        </tbody>
                        </table>
                        @endforeach
                    </div>
                    
                    <div class="col-md-5 card today-card">
                        <h1 class="title"> LIVE MATCH </h3>
                        @foreach($liveMatchs->matches as $match)
                        <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">{{ $match->competition->name }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="match-score">
                            <td  scope="row"> {{ $match->homeTeam->name }} <a href="#" target="popup" onClick="window.open('/match/{{$match->id}}','popup',width=600,height=300); return false;">( {{ $match->score->fullTime->homeTeam }} - {{ $match->score->fullTime->awayTeam }} )</a> {{ $match->awayTeam->name }}</td>
                            
                            </tr>
                        </tbody>
                        </table>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.running_text')

@endsection