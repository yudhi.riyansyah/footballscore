@extends('layouts.master_admin')
@section('content')
<div id="content" class="match_wallpaper" >
    <!-- Topbar -->
    @include('partials.admin_navbar_mod')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        {{-- ==== --}}
        <div class=" today_match card main-card">
            <div class="container">
                <h1 class="title" allign="center">{{ $todayStats->match->homeTeam->name }} VS {{ $todayStats->match->awayTeam->name }}</h1>

                <h5> Venue   : {{ $todayStats->match->venue }}</h5>
                <h6> Referee : {{ $todayStats->match->referees[0]->name }}</h6>

                <table class="table" allign="center">
                    <thead class="thead-light">
                        <tr>
                        <th> Total Matches: {{ $todayStats->head2head->numberOfMatches }}</th>
                        <th scope="col">Head to Head</th>
                        <th> Total Goals: {{ $todayStats->head2head->totalGoals }}</th>
                        </tr>
                    </thead>
                    <tbody class="match-score">
                        <tr>
                            <td>{{ $todayStats->head2head->homeTeam->wins }}</td>
                            <td>Wins</td>
                            <td>{{ $todayStats->head2head->awayTeam->wins }}</td>
                        </tr>
                        <tr>
                            <td>{{ $todayStats->head2head->homeTeam->draws }}</td>
                            <td>Draws</td>
                            <td>{{ $todayStats->head2head->awayTeam->draws }}</td>
                        </tr>
                        <tr>
                            <td>{{ $todayStats->head2head->homeTeam->losses }}</td>
                            <td>Losses</td>
                            <td>{{ $todayStats->head2head->awayTeam->losses }}</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>  

    </div>
</div> 
  
@include('partials.running_text')
@endsection


