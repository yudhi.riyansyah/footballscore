<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HighlightVideo;
use Illuminate\Support\Facades\DB;
use Alert;
use GuzzleHttp\Client;

class FavouriteController extends Controller
{
    //
    public function index($user_id)
    {
        $client = new Client();

        // $favouriteVideos = HighlightVideo::where('user_id', $user_id)
        // ->get()
        // ->simplePaginate(6);

        // dd($favouriteVideos);
        // $favouriteVideos = HighlightVideo::find($user_id)->get();
        // $favouriteVideos = DB::select('select * from highlight_videos where user_id = ?', [$user_id]);
        $favouriteVideos = DB::table('highlight_videos')->where('user_id', $user_id)->Paginate(6);

        // $token = env('NEWS_TOKEN');
        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());

        return view('favourites.index', compact('favouriteVideos', 'news'));
    }

    public function destroy($user_id, $id)
    {
        // $favouriteVideos = HighlightVideo::all();
        // dd($favouriteVideos);

        // $favouriteVideos = HighlightVideo::find($user_id)->get();
        // return view('favourites.index', compact('favouriteVideos'));

        HighlightVideo::destroy($id);
        Alert::success('Deleted Successcully');
        return redirect('favourite/' . $user_id);
    }
}
