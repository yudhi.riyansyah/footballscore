<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class NewsController extends Controller
{
    public function news($country)
    {
        $client = new Client();
        // $token = env('NEWS_TOKEN');
        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uri = "https://newsapi.org/v2/top-headlines?country=" . $country . "&apiKey=" . $news_token . "&category=sports";
        $response = $client->get($uri);
        $news = json_decode($response->getBody()->getContents());

        return view('news.index', compact('news'));
        // return redirect('#runningText', compact('news'));
    }
}
