<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class StandingsController extends Controller
{
    //
    public function index($id)
    {

        // $token = env('FOOTBALL_TOKEN');
        $token = '3fe9c9fe67564d5e8b6cd6e03ffac443';
        $client = new Client();

        $uri = "http://api.football-data.org/v2/competitions/" . $id . "/standings";
        $header = array('headers' => array('X-Auth-Token' =>  $token));
        $response = $client->get($uri, $header);
        $leagueStandings = json_decode($response->getBody()->getContents());

        // $token = env('NEWS_TOKEN');
        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());

        return view('standings.index', compact('leagueStandings',  'news'));
        // return view('standings.index', compact('leagueStandings'));
    }

    public function team_details($id)
    {
        $client = new Client();
        // $token = env('FOOTBALL_TOKEN');
        $token = '3fe9c9fe67564d5e8b6cd6e03ffac443';
        $uri = "http://api.football-data.org/v2/teams/" . $id;
        $header = array('headers' => array('X-Auth-Token' =>  $token));
        $response = $client->get($uri, $header);
        $team_details = json_decode($response->getBody()->getContents());

        // $token = env('NEWS_TOKEN');
        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());

        return view('standings.team_details', compact('team_details',  'news'));
    }
}
