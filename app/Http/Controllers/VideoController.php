<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\HighlightVideo;
use Alert;

class VideoController extends Controller
{
    //
    public function index()
    {

        // $token = env('FOOTBALL_TOKEN');
        $token = '3fe9c9fe67564d5e8b6cd6e03ffac443';
        $client = new Client();
        $uriVideo = "https://www.scorebat.com/video-api/v1/";
        $header = array('headers' => array('X-Auth-Token' =>  $token));
        $responseVideo = $client->get($uriVideo);
        $highlightVideos = json_decode($responseVideo->getBody()->getContents());

        // $token = env('NEWS_TOKEN');
        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());

        return view('video.index', compact('highlightVideos', 'news'));
    }

    public function store(Request $request)
    {
        $highlight_videos = new HighlightVideo();
        $highlight_videos->title = $request->title;
        $highlight_videos->video_embed = $request->video_embed;
        $highlight_videos->video_url = $request->video_url;
        $highlight_videos->user_id = $request->user_id;
        $highlight_videos->save();
        // Alert::message('Robots are working!');

        Alert::success('Successcully Saved');
        return redirect('/favourite/' . $request->user_id);
        // return redirect('article/admin/index');
    }
}
