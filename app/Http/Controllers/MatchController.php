<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MatchController extends Controller
{

    public function index()
    {
        return view("match.index");
    }
    public function MatchResult()
    {
        $toDate = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime("-1 day"));
        $client = new Client();

        $uri1 = 'https://api.football-data.org/v2/matches?status=FINISHED&dateFrom=' . $fromDate . '&dateTo=' . $toDate;
        $header = array('headers' => array('X-Auth-Token' => '2c3bc39b79664710a0b8fccbd65a40da'));
        $response = $client->get($uri1, $header);
        $todayMatchs = json_decode($response->getBody()->getContents());

        $uriLive = 'https://api.football-data.org/v2/matches?status=LIVE&dateFrom=' . $fromDate . '&dateTo=' . $toDate;
        $headerLive = array('headers' => array('X-Auth-Token' => '2c3bc39b79664710a0b8fccbd65a40da'));
        $responseLive = $client->get($uriLive, $headerLive);
        $liveMatchs = json_decode($responseLive->getBody()->getContents());

        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());


        return view('match.index', compact('todayMatchs', 'liveMatchs', 'news'));
    }
    public function MatchStat($id)
    {
        $client = new Client();
        $uri = 'https://api.football-data.org/v2/matches/' . $id;
        $header = array('headers' => array('X-Auth-Token' => '2c3bc39b79664710a0b8fccbd65a40da'));
        $response = $client->get($uri, $header);
        $todayStats = json_decode($response->getBody()->getContents());

        $news_token = '54b6ae3421ed43b39921808093d40f40';
        $uriNews = "https://newsapi.org/v2/top-headlines?country=id&apiKey=" . $news_token . "&category=sports";
        $responseNews = $client->get($uriNews);
        $news = json_decode($responseNews->getBody()->getContents());

        return view('match.detail', compact('todayStats', 'news'));
    }
}
