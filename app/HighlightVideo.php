<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighlightVideo extends Model
{
    //
    protected $table = "highlight_videos";
    protected $fillable = ["title", "video_embed", "video_url", "user_id"];

    public function users()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
